let choseCar = {
  imgUrl: "./img/car/red-car.jpg",
  imgArr: [
    "./img/car/red-car.jpg",
    "./img/car/black-car.jpg",
    "./img/car/silver-car.jpg",
  ],
};

export const choseCarReducer = (state = choseCar, action) => {
  switch (action.type) {
    case "change_img":
      state.imgUrl = state.imgArr[action.id];
      return { ...state };
    default:
      return state;
  }
};
