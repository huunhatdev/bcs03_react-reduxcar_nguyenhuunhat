import { combineReducers } from "redux";
import { choseCarReducer } from "./choseCarReducer";

export const rootReducerCar = combineReducers({
  choseCarReducer,
});
