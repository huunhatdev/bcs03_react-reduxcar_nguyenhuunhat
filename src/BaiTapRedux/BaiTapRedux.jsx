import React, { Component } from "react";
import { connect } from "react-redux";

class BaiTapRedux extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-6">
            <img src={this.props.imgUrl} alt="" />
          </div>
          <div className="col-6">
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleChoseColor(0);
              }}
            >
              Red
            </button>
            <button
              className="btn btn-dark"
              onClick={() => {
                this.props.handleChoseColor(1);
              }}
            >
              Black
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.handleChoseColor(2);
              }}
            >
              Gray
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    imgUrl: state.choseCarReducer.imgUrl,
    imgArr: state.choseCarReducer.imgArr,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleChoseColor: (id) => {
      const action = {
        type: "change_img",
        id: id,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BaiTapRedux);
