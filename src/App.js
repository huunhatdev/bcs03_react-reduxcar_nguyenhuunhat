import logo from "./logo.svg";
import "./App.css";
// import BaiTapPhone from "./BaiTapPhone/BaiTapPhone";
import BaiTapShoeShop from "./BaiTapShoeShop/BaiTapShoeShop";
import BaiTapRedux from "./BaiTapRedux/BaiTapRedux";
// import BaiTap2 from "./BaiTap2/BaiTap2";
// import BaiTap1 from "./BaiTap1/BaiTap1";

function App() {
  return (
    <div className="App">
      {/* <BaiTap1 /> */}
      {/* <BaiTap2 /> */}
      {/* <BaiTapShoeShop /> */}
      {/* <BaiTapPhone /> */}
      <BaiTapRedux />
    </div>
  );
}

export default App;
